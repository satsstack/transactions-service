package cz.uhk.fim.mois.satsstack.transactionsservice.domain.util;

/**
 * @author : vanya.melnykovych
 * @since : 07.04.2021
 */
public enum TransactionType {
    BUY,
    SELL
}
