package cz.uhk.fim.mois.satsstack.transactionsservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author : vanya.melnykovych
 * @since : 07.04.2021
 */
@Data
@Document
@NoArgsConstructor
@AllArgsConstructor
public class Asset {

    @Id
    private Long id;

    private String name;

    private String shortCut;

}
