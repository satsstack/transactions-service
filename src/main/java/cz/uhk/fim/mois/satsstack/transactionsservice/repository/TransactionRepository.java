package cz.uhk.fim.mois.satsstack.transactionsservice.repository;

import cz.uhk.fim.mois.satsstack.transactionsservice.domain.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author : vanya.melnykovych
 * @since : 07.04.2021
 */
public interface TransactionRepository extends MongoRepository<Transaction, Long> {

    List<Transaction> findByUserIdOrderByDateDesc(String userId);

    List<Transaction> findByUserIdAndAssetIdOrderByDateDesc(String userId, Long assetId);

    Page<Transaction> findPaginatedByUserIdOrderByDateDesc(String userId, Pageable pageable);

    Page<Transaction> findPaginatedByUserIdAndAssetIdOrderByDateDesc(String userId, Long assetId, Pageable pageable);
}
