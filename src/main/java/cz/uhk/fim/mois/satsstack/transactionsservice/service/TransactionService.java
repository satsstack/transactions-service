package cz.uhk.fim.mois.satsstack.transactionsservice.service;

import cz.uhk.fim.mois.satsstack.transactionsservice.domain.Transaction;
import cz.uhk.fim.mois.satsstack.transactionsservice.domain.util.ErrorUtil;
import cz.uhk.fim.mois.satsstack.transactionsservice.domain.util.URLUtil;
import cz.uhk.fim.mois.satsstack.transactionsservice.repository.TransactionRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author : vanya.melnykovych
 * @since : 26.04.2021
 */
@Service
public class TransactionService {

    private final URLUtil urlUtil;

    private final TransactionRepository transactionRepository;

    private final SequenceGeneratorService sequenceGeneratorService;

    public TransactionService(URLUtil urlUtil, TransactionRepository transactionRepository, SequenceGeneratorService sequenceGeneratorService) {
        this.urlUtil = urlUtil;
        this.transactionRepository = transactionRepository;
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    public ResponseEntity<Map<String, Object>> getPaginatedByUserIdAndAssetId(String userId, Long assetId, String currency, String token, int page, int size) {
        try {
            Pageable paging = PageRequest.of(page, size);

            Page<Transaction> transactionPage = transactionRepository.findPaginatedByUserIdAndAssetIdOrderByDateDesc(userId, assetId, paging);
            return createResponse(transactionPage, currency, token);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public Object saveTransaction(Transaction transaction, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ErrorUtil.getErrorResponse(bindingResult);
        }
        transaction.setId((long) sequenceGeneratorService.getSequenceNumber(Transaction.SEQUENCE_NAME));
        return transactionRepository.save(transaction);
    }

    public ResponseEntity<Map<String, Object>> getAllPaginated(String userId, String currency, String token, int page, int size) {

        try {
            Pageable paging = PageRequest.of(page, size);

            Page<Transaction> transactionPage = transactionRepository.findPaginatedByUserIdOrderByDateDesc(userId, paging);
            return createResponse(transactionPage, currency, token);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public List<Transaction> getByUserIdAndAssetId(String userId, Long assetId, String token, String currency) {
        return recalculatePriceByCurrency(transactionRepository.findByUserIdAndAssetIdOrderByDateDesc(userId, assetId),
                currency, token);
    }

    private ResponseEntity<Map<String, Object>> createResponse(Page<Transaction> transactionPage, String currency, String token) {
        Map<String, Object> response = new HashMap<>();
        response.put("transactions", recalculatePriceByCurrency(transactionPage.getContent(), currency, token));
        response.put("currentPage", transactionPage.getNumber());
        response.put("totalItems", transactionPage.getTotalElements());
        response.put("totalPages", transactionPage.getTotalPages());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private List<Transaction> recalculatePriceByCurrency(List<Transaction> transactions, String currency, String token) {
        double currencyValue = getCurrency(currency, token);
        return transactions.stream()
                .map(t -> new Transaction(t.getId(), t.getFee() * currencyValue,
                        t.getQuantity(), t.getNote(), t.getTransactionType(),
                        t.getPricePerAsset().multiply(BigDecimal.valueOf(currencyValue)),
                        t.getDate(), t.getUserId(), t.getAsset()))
                .collect(Collectors.toList());
    }

    private double getCurrency(String currency, String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", token);
        HttpEntity<Double> request = new HttpEntity<>(headers);
        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromHttpUrl(urlUtil.buildAnalysisServiceBaseURL() + URLUtil.STATISTIC_CONTROLLER_URL + "/currency")
                .queryParam("currency", currency);
        ResponseEntity<Double> response = new RestTemplate()
                .exchange(uriBuilder.toUriString(), HttpMethod.GET, request, Double.class);
        return Objects.requireNonNull(response.getBody());
    }
}
