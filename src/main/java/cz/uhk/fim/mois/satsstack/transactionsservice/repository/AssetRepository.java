package cz.uhk.fim.mois.satsstack.transactionsservice.repository;

import cz.uhk.fim.mois.satsstack.transactionsservice.domain.Asset;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author : vanya.melnykovych
 * @since : 07.04.2021
 */
public interface AssetRepository extends MongoRepository<Asset, Long> {
}
