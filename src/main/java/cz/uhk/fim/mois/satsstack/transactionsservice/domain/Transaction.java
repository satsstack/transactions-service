package cz.uhk.fim.mois.satsstack.transactionsservice.domain;

import cz.uhk.fim.mois.satsstack.transactionsservice.domain.util.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author : vanya.melnykovych
 * @since : 07.04.2021
 */
@Data
@Document
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {

    @Transient
    public static final String SEQUENCE_NAME = "transaction_sequence";

    @Id
    private Long id;

    private double fee;

    @NotNull
    @Positive
    private double quantity;

    private String note;

    @NotNull
    private TransactionType transactionType;

    @NotNull
    private BigDecimal pricePerAsset;

    @NotNull
    @PastOrPresent
    private Date date;

    @NotNull
    private String userId;

    @NotNull
    private Asset asset;
}
