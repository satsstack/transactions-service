package cz.uhk.fim.mois.satsstack.transactionsservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author : vanya.melnykovych
 * @since : 27.04.2021
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "db_sequence")
public class DbSequence {
    
    @Id
    private String  id;
    private int seq;
}
