package cz.uhk.fim.mois.satsstack.transactionsservice.controller;

import cz.uhk.fim.mois.satsstack.transactionsservice.domain.Asset;
import cz.uhk.fim.mois.satsstack.transactionsservice.repository.AssetRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author : vanya.melnykovych
 * @since : 07.04.2021
 */
@RestController
@RequestMapping("rest/api/asset")
public class AssetController {

    private final AssetRepository assetRepository;

    public AssetController(AssetRepository assetRepository) {
        this.assetRepository = assetRepository;
    }

    @GetMapping("/all")
    public List<Asset> getAll() {
        return assetRepository.findAll();
    }
}
