package cz.uhk.fim.mois.satsstack.transactionsservice.controller;

import cz.uhk.fim.mois.satsstack.transactionsservice.domain.Transaction;
import cz.uhk.fim.mois.satsstack.transactionsservice.domain.util.ErrorUtil;
import cz.uhk.fim.mois.satsstack.transactionsservice.repository.TransactionRepository;
import cz.uhk.fim.mois.satsstack.transactionsservice.service.TransactionService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * @author : vanya.melnykovych
 * @since : 07.04.2021
 */
@RestController
@RequestMapping("rest/api/transaction")
public class TransactionController {

    private final TransactionRepository transactionRepository;

    private final TransactionService transactionService;

    public TransactionController(TransactionRepository transactionRepository, TransactionService transactionService) {
        this.transactionRepository = transactionRepository;
        this.transactionService = transactionService;
    }

    @GetMapping("/find/{userId}")
    public List<Transaction> getAll(@PathVariable String userId) {
        return transactionRepository.findByUserIdOrderByDateDesc(userId);
    }

    @GetMapping("/find/paginated/{userId}")
    public ResponseEntity<Map<String, Object>> getAllPaginated(@PathVariable String userId,
                                             @RequestParam(required = false) String currency,
                                             @RequestHeader("Authorization") String token,
                                             @RequestParam(defaultValue = "0") int page,
                                             @RequestParam(defaultValue = "10") int size) {
        return transactionService.getAllPaginated(userId, currency, token, page, size);
    }

    @GetMapping("/find/asset/{userId}/{assetId}")
    public List<Transaction> getUserTransactionsByAsset(@PathVariable String userId,
                                                        @PathVariable Long assetId,
                                                        @RequestHeader("Authorization") String token,
                                                        @RequestParam(required = false) String currency) {
        return transactionService.getByUserIdAndAssetId(userId, assetId, token, currency);
    }

    @PostMapping("/save")
    public Object saveTransaction(@RequestBody @Valid Transaction transaction, BindingResult bindingResult) {
        return transactionService.saveTransaction(transaction, bindingResult);
    }

    @PutMapping("/update")
    public Object updateTransaction(@RequestBody @Valid Transaction transaction, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ErrorUtil.getErrorResponse(bindingResult);
        }
        return transactionRepository.save(transaction);
    }

    @DeleteMapping("/delete/{transactionId}")
    public void deleteTransaction(@PathVariable Long transactionId) {
        transactionRepository.deleteById(transactionId);
    }

    @DeleteMapping("/delete/{userId}/{assetId}")
    public List<Transaction> deleteTransactionByAssetAndUserId(@PathVariable String userId, @PathVariable Long assetId) {
        List<Transaction> transactions = transactionRepository.findByUserIdAndAssetIdOrderByDateDesc(userId, assetId);
        transactionRepository.deleteAll(transactions);
        return transactions;
    }

    @GetMapping("/find/paginated/asset/{userId}/{assetId}")
    public ResponseEntity<Map<String, Object>> getPaginatedByUserIdAndAssetId(@PathVariable String userId,
                                                                              @PathVariable Long assetId,
                                                                              @RequestParam(required = false) String currency,
                                                                              @RequestHeader("Authorization") String token,
                                                                              @RequestParam(defaultValue = "0") int page,
                                                                              @RequestParam(defaultValue = "10") int size) {
        return transactionService.getPaginatedByUserIdAndAssetId(userId, assetId, currency, token, page, size);
    }
}
