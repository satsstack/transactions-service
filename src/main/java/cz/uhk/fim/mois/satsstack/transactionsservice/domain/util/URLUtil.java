package cz.uhk.fim.mois.satsstack.transactionsservice.domain.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author : vanya.melnykovych
 * @since : 08.04.2021
 */
@Component
public class URLUtil {

    @Value("${server.user.service.host}")
    private String userServiceHost;

    @Value("${server.user.service.port}")
    private String userServicePort;

    @Value("${server.analysis.service.host}")
    private String analysisServiceHost;

    @Value("${server.analysis.service.port}")
    private String analysisServicePort;

    public static final String MONGODB_PREFIX = "mongodb+srv";
    public static final String MONGODB_CLUSTER_NAME = "satsstack-mongodb.brheq.mongodb.net";
    public static final String MONGODB_PARAMETERS = "myFirstDatabase?retryWrites=true&w=majority";

    public static final String USER_CONTROLLER_URL = "rest/api/user";

    public static final String STATISTIC_CONTROLLER_URL = "rest/api/statistics";

    public String buildAnalysisServiceBaseURL() {
        StringBuilder builder = new StringBuilder()
                .append("https://")
                .append(analysisServiceHost);
        if (analysisServicePort != null && !analysisServicePort.isEmpty()) {
            builder.append(":")
                    .append(analysisServicePort);
        }
        builder.append("/");
        return builder.toString();
    }

    public String buildUserServiceBaseURL() {
        StringBuilder builder = new StringBuilder()
                .append("https://")
                .append(userServiceHost);
        if (userServicePort != null && !userServicePort.isEmpty()) {
            builder.append(":")
                    .append(userServicePort);
        }
        builder.append("/");
        return builder.toString();
    }
}
