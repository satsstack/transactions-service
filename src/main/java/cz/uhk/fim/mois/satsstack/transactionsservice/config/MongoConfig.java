package cz.uhk.fim.mois.satsstack.transactionsservice.config;

import com.mongodb.Block;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.connection.SslSettings;
import cz.uhk.fim.mois.satsstack.transactionsservice.domain.util.URLUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.Collections;

/**
 * @author : vanya.melnykovych
 * @since : 07.04.2021
 */
@Configuration
public class MongoConfig extends AbstractMongoClientConfiguration {

    @Value("${mongodb.database}")
    private String database;

    @Value("${mongodb.database.user}")
    private String databaseUser;

    @Value("${mongodb.database.user.password}")
    private String databaseUserPassword;

    @Override
    protected String getDatabaseName() {
        return database;
    }

    @Override
    public MongoClient mongoClient() {
        ConnectionString connectionString = new ConnectionString(buildConnectionURL());
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .applyToSslSettings(getSSLSetting())
                .build();

        return MongoClients.create(mongoClientSettings);
    }

    @Override
    public Collection getMappingBasePackages() {
        return Collections.singleton("cz.uhk.fim.mois.satsstack.transactionservice");
    }

    private SSLContext getSSLContext() throws NoSuchAlgorithmException, KeyManagementException {
        final SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        sslContext.init(null, null, new SecureRandom());
        return sslContext;
    }

    private Block<SslSettings.Builder> getSSLSetting() {
        return builder -> {
            try {
                builder.enabled(true).context(getSSLContext());
            }  catch (NoSuchAlgorithmException | KeyManagementException e) {
                builder.enabled(false);
            }
        };
    }

    private String buildConnectionURL() {
        return URLUtil.MONGODB_PREFIX +
                "://" +
                databaseUser +
                ":" +
                databaseUserPassword +
                "@" +
                URLUtil.MONGODB_CLUSTER_NAME +
                "/" +
                URLUtil.MONGODB_PARAMETERS;
    }
}
